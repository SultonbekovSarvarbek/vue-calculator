import './assets/main.css';
import 'ant-design-vue/dist/reset.css';

import axios from 'axios';
axios.defaults.baseURL = 'http://164.92.184.3:8000';

// /start_task/

import Antd from 'ant-design-vue';

import { createApp } from 'vue';
import App from './App.vue';

createApp(App).use(Antd).mount('#app');